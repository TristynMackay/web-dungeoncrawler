﻿using DungeonCrawler.Models;
using DungeonCrawler.Models.Game;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Services;

namespace DungeonCrawler.Controllers
{
    public class HomeController : Controller
    {
        DCRepository dc = new DCRepository();

        // Home Page.
        public ActionResult Index()
        {
            return View();
        }


        #region - Information Pages.
        public ActionResult About()
        {
            ViewBag.Message = "About Dungeon Crawler";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Details";

            return View();
        }

        public ActionResult Help()
        {
            ViewBag.Message = "Helpful Guides and Tips";

            return View();
        }
        #endregion


        #region - Player Pages.
        [Authorize]
        public ActionResult CharacterSelect()
        {
            ViewBag.Message = "Select a Character";

            ViewData.Model = dc.GetCharacters(User.Identity.GetUserName());

            return View();
        }

        [Authorize]
        public ActionResult CharacterCreation()
        {
            ViewBag.Message = "Create a Character";

            return View();
        }

        [Authorize]
        public ActionResult Game(int characterID)
        {
            string user = User.Identity.Name.Split('@')[0];
            
            ViewData.Model = dc.GetCharacterData(characterID, User.Identity.Name);

            ViewBag.Title = "Username : " + user;
            ViewBag.Message = "Name : ";

            return View();
        }
        #endregion


        #region - Create Page Views.
        [Authorize(Roles = "Owner")]
        public ActionResult CreateRace()
        {
            ViewBag.Message = "Create a Race.";

            ViewData.Model = new RacesView { Race = new Race(), Races = dc.GetAllRaces() };

            return View();
        }

        [Authorize(Roles = "Owner")]
        public ActionResult CreateClass()
        {
            ViewBag.Message = "Create a Class.";

            ViewData.Model = new ClassesView { Class = new Class(), Classes = dc.GetAllClasses() };

            return View();
        }


        [Authorize(Roles = "Owner")]
        public ActionResult CreateCurrency()
        {
            ViewBag.Message = "Create a Currency.";

            ViewData.Model = new CurrenciesView { Currency = new Currency(), Currencies = dc.GetAllCurrencies(), CurrencyRate = new CurrencyConversionRate(), CurrencyRates = dc.GetAllCurrencyRates() };

            return View();
        }

        
        [Authorize(Roles = "Owner")]
        public ActionResult CreateZone()
        {
            ViewBag.Message = "Create a Zone.";

            ViewData.Model = new ZonesView { Zone = new Zone(), Zones = dc.GetAllZones() };

            return View();
        }


        [Authorize(Roles = "Owner")]
        public ActionResult CreateItemType()
        {
            ViewBag.Message = "Create an Item Type.";

            ViewData.Model = new ItemTypesView { ItemType = new ItemType(), ItemTypes = dc.GetAllItemTypes() };

            return View();
        }

        [Authorize(Roles = "Owner")]
        public ActionResult CreateItemTier()
        {
            ViewBag.Message = "Create an Item Tier.";

            ViewData.Model = new ItemTiersView { ItemTier = new ItemTier(), ItemTiers = dc.GetAllItemTiers() };

            return View();
        }

        
        [Authorize(Roles = "Owner")]
        public ActionResult CreateDamageType()
        {
            ViewBag.Message = "Create a Damage Type.";

            ViewData.Model = new DamageTypesView { DamageType = new DamageType(), DamageTypes = dc.GetAllDamageTypes() };

            return View();
        }


        [Authorize(Roles = "Owner")]
        public ActionResult CreateSlotType()
        {
            ViewBag.Message = "Create a Slot Type.";

            ViewData.Model = new SlotTypesView { SlotType = new SlotType(), SlotTypes = dc.GetAllSlotTypes() };

            return View();
        }


        [Authorize(Roles = "Owner")]
        public ActionResult CreateArmourType()
        {
            ViewBag.Message = "Create an Armour Type.";

            ViewData.Model = new ArmourTypesView { ArmourType = new ArmourType(), ArmourTypes = dc.GetAllArmourTypes(), DamageTypes = dc.GetAllDamageTypes(), ArmourWeaknesses = dc.GetAllArmourWeaknesses(), ArmourStrengths = dc.GetAllArmourStrengths() };

            return View();
        }


        [Authorize(Roles = "Owner")]
        public ActionResult CreateWeaponType()
        {
            ViewBag.Message = "Create a Weapon Type.";

            ViewData.Model = new WeaponTypesView { WeaponType = new WeaponType(), WeaponTypes = dc.GetAllWeaponTypes(), DamageTypes = dc.GetAllDamageTypes(), WeaponDamageTypes = dc.GetAllWeaponDamageTypes() };

            return View();
        }

        // Still need to finish (loots/lootTables).
        [Authorize(Roles = "Owner")]
        public ActionResult CreateItem()
        {
            ViewBag.Message = "Create an Item.";

            ViewData.Model = new AllItemsView { Items = dc.GetAllItems(),
                                                ItemStats = dc.GetAllItemStats(),
                                                ItemTiers = dc.GetAllItemTiers(),
                                                ItemTypes = dc.GetAllItemTypes(),
                                                SlotTypes = dc.GetAllSlotTypes(),
                                                DamageTypes = dc.GetAllDamageTypes(),
                                                ArmourTypes = dc.GetAllArmourTypes(),
                                                ArmourStrengths = dc.GetAllArmourStrengths(),
                                                ArmourWeaknesses = dc.GetAllArmourWeaknesses(),
                                                WeaponTypes = dc.GetAllWeaponTypes(),
                                                WeaponDamageTypes = dc.GetAllWeaponDamageTypes(),
                                                Currencies = dc.GetAllCurrencies(),
                                                CurrencyConversionRates = dc.GetAllCurrencyRates(),
                                                Classes = dc.GetAllClasses() };

            return View();
        }
        #endregion


        #region - POST Methods.
        [HttpPost]
        public ActionResult SetCharacter(string id)
        {
            int ID = -1;
            if (int.TryParse(id, out ID) == true)
            {

                return RedirectToAction("Game", new { characterID = id });
            }
            else
            {
                return View();
            }
        }


        #region - CREATE POST Methods.
        [HttpPost]
        public ActionResult CreateRaceByName(string raceName)
        {
            dc.CreateRace(raceName);

            return Redirect("CreateRace");
        }


        [HttpPost]
        public ActionResult CreateClassByName(string className)
        {
            dc.CreateClass(className);

            return Redirect("CreateClass");
        }


        [HttpPost]
        public ActionResult CreateZoneByName(string zoneName, string zoneDescription)
        {
            dc.CreateZone(zoneName, zoneDescription);

            return Redirect("CreateZone");
        }


        [HttpPost]
        public ActionResult CreateCurrencyByName(string currencyName)
        {
            dc.CreateCurrency(currencyName);

            return Redirect("CreateCurrency");
        }

        [HttpPost]
        public ActionResult CreateCurrencyRateByID(int currencyFrom, int currencyTo, float rate)
        {
            dc.CreateCurrencyRate(currencyFrom, currencyTo, rate);

            return Redirect("CreateCurrency");
        }
        
        [HttpPost]
        public ActionResult CreateCurrencyRateByName(string currencyFrom, string currencyTo, float rate)
        {
            dc.CreateCurrencyRate(currencyFrom, currencyTo, rate);

            return Redirect("CreateCurrency");
        }


        [HttpPost]
        public ActionResult CreateItemTypeByName(string itemTypeName)
        {
            dc.CreateItemType(itemTypeName);

            return Redirect("CreateItemType");
        }
        
        [HttpPost]
        public ActionResult CreateItemTierByName(string itemTierName)
        {
            dc.CreateItemTier(itemTierName);

            return Redirect("CreateItemTier");
        }


        [HttpPost]
        public ActionResult CreateSlotTypeByName(string slotTypeName)
        {
            dc.CreateSlotType(slotTypeName);

            return Redirect("CreateSlotType");
        }


        [HttpPost]
        public ActionResult CreateDamageTypeByName(string damageTypeName)
        {
            dc.CreateDamageType(damageTypeName);

            return Redirect("CreateDamageType");
        }


        [HttpPost]
        public ActionResult CreateArmourTypeByName(string armourTypeName)
        {
            dc.CreateArmourType(armourTypeName);

            return Redirect("CreateArmourType");
        }

        [HttpPost]
        public ActionResult CreateArmourWeaknessByName(string armourTypeName, string weaknessName, float amount)
        {
            int armourTypeID = dc.GetArmourType(armourTypeName).armourTypeID;
            int weaknessID = dc.GetDamageType(weaknessName).damageTypeID;

            dc.CreateArmourWeakness(armourTypeID, weaknessID, amount);

            return Redirect("CreateArmourType");
        }

        [HttpPost]
        public ActionResult CreateArmourWeaknessByID(int armourTypeID, int weaknessID, float amount)
        {
            dc.CreateArmourWeakness(armourTypeID, weaknessID, amount);

            return Redirect("CreateArmourType");
        }

        [HttpPost]
        public ActionResult CreateArmourStrengthByName(string armourTypeName, string strengthName, float amount)
        {
            int armourTypeID = dc.GetArmourType(armourTypeName).armourTypeID;
            int strengthID = dc.GetDamageType(strengthName).damageTypeID;

            dc.CreateArmourStrength(armourTypeID, strengthID, amount);

            return Redirect("CreateArmourType");
        }

        [HttpPost]
        public ActionResult CreateArmourStrengthByID(int armourTypeID, int strengthID, float amount)
        {
            dc.CreateArmourStrength(armourTypeID, strengthID, amount);

            return Redirect("CreateArmourType");
        }


        [HttpPost]
        public ActionResult CreateWeaponTypeByName(string weaponTypeName)
        {
            dc.CreateWeaponType(weaponTypeName);

            return Redirect("CreateWeaponType");
        }

        [HttpPost]
        public ActionResult CreateWeaponDamageTypeByName(string weaponTypeName, string damageTypeName)
        {
            int weaponTypeID = dc.GetWeaponType(weaponTypeName).weaponTypeID;
            int damageTypeID = dc.GetDamageType(damageTypeName).damageTypeID;

            dc.CreateWeaponDamageType(weaponTypeID, damageTypeID);

            return Redirect("CreateWeaponType");
        }

        [HttpPost]
        public ActionResult CreateWeaponDamageTypeByID(int weaponTypeID, int damageTypeID)
        {
            dc.CreateWeaponDamageType(weaponTypeID, damageTypeID);

            return Redirect("CreateWeaponType");
        }
        #endregion
        #endregion


        private float roundTo3Decimals(float number)
        {
            string[] strings = System.Math.Round(number, 3).ToString().Split('.');

            string newDecimalString = strings[1].Substring(0, 3);

            string resultString  = strings[1] + "." + newDecimalString;

            float result = 0;

            try
            {
                result = float.Parse(resultString);
            }
            catch
            {

            }
            
            return result;
        }
    }
}