﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class ItemTypesView
    {
        public ItemType ItemType { get; set; }
        public IList<ItemType> ItemTypes { get; set; }
    }
}