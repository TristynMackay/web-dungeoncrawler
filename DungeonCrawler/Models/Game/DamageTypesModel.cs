﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class DamageTypesView
    {
        public DamageType DamageType { get; set; }
        public IList<DamageType> DamageTypes { get; set; }
    }
}