﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class CurrenciesView
    {
        public Currency Currency { get; set; }
        public IList<Currency> Currencies { get; set; }
        public CurrencyConversionRate CurrencyRate { get; set; }
        public IList<CurrencyConversionRate> CurrencyRates { get; set; }
    }
}