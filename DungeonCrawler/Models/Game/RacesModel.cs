﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class RacesView
    {
        public Race Race { get; set; }
        public IList<Race> Races { get; set; }
    }
}