﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class ClassesView
    {
        public Class Class { get; set; }
        public IList<Class> Classes { get; set; }
    }

    public class CharacterView
    {
        public int CharacterID { get; set; }

        public string CharacterName { get; set; }

        public string Gender { get; set; }

        public string ClassName { get; set; }

        public string RaceName { get; set; }
    }

    public class CharacterData
    {
        public Character Character { get; set; }
        
        public Inventory Inventory { get; set; }

        public Equipment Equipment { get; set; }

        public CharacterStat CharacterStat { get; set; }

        public string ClassName { get; set; }

        public string RaceName { get; set; }

        public Bank Bank { get; set; }

        public Wallet Wallet { get; set; }
    }
}