﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class ItemTiersView
    {
        public ItemTier ItemTier { get; set; }
        public IList<ItemTier> ItemTiers { get; set; }
    }
}