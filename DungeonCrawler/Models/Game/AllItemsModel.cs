﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class AllItemsView
    {
        public IList<Item> Items { get; set; }
        public IList<ItemStat> ItemStats { get; set; }

        public IList<ItemTier> ItemTiers { get; set; }

        public IList<ItemType> ItemTypes { get; set; }

        public IList<SlotType> SlotTypes { get; set; }

        public IList<DamageType> DamageTypes { get; set; }

        public IList<ArmourType> ArmourTypes { get; set; }
        public IList<ArmourStrength> ArmourStrengths { get; set; }
        public IList<ArmourWeakness> ArmourWeaknesses { get; set; }

        public IList<WeaponType> WeaponTypes { get; set; }
        public IList<WeaponDamageType> WeaponDamageTypes { get; set; }

        public IList<Currency> Currencies { get; set; }
        public IList<CurrencyConversionRate> CurrencyConversionRates { get; set; }

        public IList<Class> Classes { get; set; }
    }
}