﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class WeaponTypesView
    {
        public WeaponType WeaponType { get; set; }
        public IList<DamageType> DamageTypes { get; set; }
        public IList<WeaponType> WeaponTypes { get; set; }
        public IList<WeaponDamageType> WeaponDamageTypes { get; set; }
    }
}