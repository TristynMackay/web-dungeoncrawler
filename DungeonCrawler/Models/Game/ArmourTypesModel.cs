﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class ArmourTypesView
    {
        public ArmourType ArmourType { get; set; }
        public IList<DamageType> DamageTypes { get; set; }
        public IList<ArmourWeakness> ArmourWeaknesses { get; set; }
        public IList<ArmourStrength> ArmourStrengths { get; set; }
        public IList<ArmourType> ArmourTypes { get; set; }
    }
}