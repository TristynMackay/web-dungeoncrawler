﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class AllCraftsView
    {
        public IList<Item> Items { get; set; }

        public IList<CraftType> CraftTypes { get; set; }

        public IList<Recipe> Recipes { get; set; }
        public IList<CraftingReagent> Reagents { get; set; }

        public IList<AlchemyRecipe> AlchemyRecipes { get; set; }
        public IList<ArmoursmithingRecipe> ArmoursmithingRecipes { get; set; }
        public IList<CookingRecipe> CookingRecipes { get; set; }
        public IList<EnchantingRecipe> EnchantingRecipes { get; set; }
        public IList<FishingRecipe> FishingRecipes { get; set; }
        public IList<HerbalismRecipe> HerbalismRecipes { get; set; }
        public IList<JewelcraftingRecipe> JewelcraftingRecipes { get; set; }
        public IList<LeatherworkingRecipe> LeatherworkingRecipes { get; set; }
        public IList<MiningRecipe> MiningRecipes { get; set; }
        public IList<RunecraftingRecipe> RunecraftingRecipes { get; set; }
        public IList<SkinningRecipe> SkinningRecipe { get; set; }
        public IList<TailoringRecipe> TailoringRecipes { get; set; }
    }
}