﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class SlotTypesView
    {
        public SlotType SlotType { get; set; }
        public IList<SlotType> SlotTypes { get; set; }
    }
}