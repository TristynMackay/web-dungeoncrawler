﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class AllCoreView
    {
        public IList<Race> Races { get; set; }
        public IList<Class> Classes { get; set; }
        public IList<Currency> Currencies { get; set; }
        public IList<DamageType> DamageTypes { get; set; }
        public IList<Faction> Factions { get; set; }
        public IList<Zone> Zones { get; set; }
        public IList<Guild> Guilds { get; set; }
    }
}