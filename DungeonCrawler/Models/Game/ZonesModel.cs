﻿

using System;
using System.Collections.Generic;

namespace DungeonCrawler.Models.Game
{
    public class ZonesView
    {
        public Zone Zone { get; set; }
        public IList<Zone> Zones { get; set; }
    }
}