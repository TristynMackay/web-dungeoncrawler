//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DungeonCrawler.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Character
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Character()
        {
            this.CharacterFactions = new HashSet<CharacterFaction>();
            this.Guilds = new HashSet<Guild>();
            this.InventorySlots = new HashSet<InventorySlot>();
        }
    
        public int characterID { get; set; }
        public string userID { get; set; }
        public string characterName { get; set; }
        public string gender { get; set; }
        public int classID { get; set; }
        public int characterRaceID { get; set; }
        public int mailBoxID { get; set; }
        public Nullable<int> guildID { get; set; }
        public Nullable<int> groupID { get; set; }
        public int zoneID { get; set; }
        public double locationX { get; set; }
        public double locationY { get; set; }
        public double locationZ { get; set; }
        public System.DateTime dateCreated { get; set; }
        public System.DateTime lastLoginDate { get; set; }
    
        public virtual AlchemyCraft AlchemyCraft { get; set; }
        public virtual ArmoursmithingCraft ArmoursmithingCraft { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CharacterFaction> CharacterFactions { get; set; }
        public virtual CharacterRace CharacterRace { get; set; }
        public virtual CharacterStat CharacterStat { get; set; }
        public virtual CookingCraft CookingCraft { get; set; }
        public virtual EnchantingCraft EnchantingCraft { get; set; }
        public virtual Equipment Equipment { get; set; }
        public virtual FishingCraft FishingCraft { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Guild> Guilds { get; set; }
        public virtual GuildMember GuildMember { get; set; }
        public virtual HerbalismCraft HerbalismCraft { get; set; }
        public virtual Inventory Inventory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventorySlot> InventorySlots { get; set; }
        public virtual JewelcraftingCraft JewelcraftingCraft { get; set; }
        public virtual LeatherworkingCraft LeatherworkingCraft { get; set; }
        public virtual MiningCraft MiningCraft { get; set; }
        public virtual RunecraftingCraft RunecraftingCraft { get; set; }
        public virtual SkinningCraft SkinningCraft { get; set; }
        public virtual TailoringCraft TailoringCraft { get; set; }
        public virtual Class Class { get; set; }
        public virtual Zone Zone { get; set; }
    }
}
