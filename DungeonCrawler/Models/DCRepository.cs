﻿using DungeonCrawler.Models.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DungeonCrawler.Models
{
    public class DCRepository
    {
        DC_Accounts_Entities db_Accounts = new DC_Accounts_Entities();
        DC_Game_Entities db_Game = new DC_Game_Entities();


        #region - Character methods.
        public IList<CharacterView> GetCharacters(string userName)
        {
            string userID = (from x in db_Accounts.AspNetUsers
                             where x.UserName == userName
                             select x.Id).SingleOrDefault();

            IList<Character> temp = (from x in db_Game.Characters
                                     where x.userID == userID
                                     select x).ToList();

            IList<CharacterView> characters = new List<CharacterView>();

            foreach (Character c in temp)
            {
                characters.Add(new CharacterView { CharacterID = c.characterID, CharacterName = c.characterName, Gender = c.gender, RaceName = GetRaceName(c.characterRaceID), ClassName = GetClassName(c.classID) });
            }

            return characters;
        }


        public CharacterData GetCharacterData(int charID, string userName)
        {
            string userID = (from x in db_Accounts.AspNetUsers
                             where x.UserName == userName
                             select x.Id).SingleOrDefault();

            // Character variables.
            Character character = GetCharacter(charID, userID);

            Inventory inventory = GetInventory(charID);

            Equipment equipment = GetEquipment(charID);

            CharacterStat characterStats = GetCharacterStat(charID);

            string className = GetClassName(character.classID);

            string raceName = GetRaceName(character.characterRaceID);

            // User variables.
            Bank bank = GetBank(userID);

            Wallet wallet = GetWallet(userID);

            return new CharacterData { Character = character, Inventory = inventory, Equipment = equipment, CharacterStat = characterStats, ClassName = className, RaceName = raceName, Bank = bank, Wallet = wallet };
        }
        #endregion


        #region - Get Singular Methods.
        private Character GetCharacter(int charID, string userID)
        {
            return (from x in db_Game.Characters
                    where x.characterID == charID && x.userID == userID
                    select x).SingleOrDefault();
        }

        private Character GetCharacter(int charID)
        {
            return (from x in db_Game.Characters
                    where x.characterID == charID
                    select x).SingleOrDefault();
        }


        private Inventory GetInventory(int charID)
        {
            return (from x in db_Game.Inventories
                    where x.characterID == charID
                    select x).SingleOrDefault();
        }


        private Equipment GetEquipment(int charID)
        {
            return (from x in db_Game.Equipments
                    where x.characterID == charID
                    select x).SingleOrDefault();
        }


        private CharacterStat GetCharacterStat(int charID)
        {
            return (from x in db_Game.CharacterStats
                    where x.characterID == charID
                    select x).SingleOrDefault();
        }


        private Bank GetBank(string userID)
        {
            return (from x in db_Game.Banks
                    where x.userID == userID
                    select x).SingleOrDefault();
        }


        private Wallet GetWallet(string userID)
        {
            return (from x in db_Game.Wallets
                    where x.userID == userID
                    select x).SingleOrDefault();
        }


        private string GetClassName(int classID)
        {
            return (from x in db_Game.Classes
                    where x.classID == classID
                    select x.className).SingleOrDefault();
        }


        private string GetRaceName(int raceID)
        {
            return (from x in db_Game.Races
                    where x.raceID == raceID
                    select x.raceType).SingleOrDefault();
        }


        /// <summary>
        /// Get Currency by ID.
        /// </summary>
        /// <param name="currencyID"></param>
        /// <returns></returns>
        private Currency GetCurrency(int currencyID)
        {
            return (from x in db_Game.Currencies
                    where x.currencyID == currencyID
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Currency by name.
        /// </summary>
        /// <param name="currencyName"></param>
        /// <returns></returns>
        private Currency GetCurrency(string currencyName)
        {
            return (from x in db_Game.Currencies
                    where x.name == currencyName
                    select x).SingleOrDefault();
        }


        /// <summary>
        /// Get Item by ID.
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns></returns>
        private Item GetItem(int itemID)
        {
            return (from x in db_Game.Items
                    where x.itemID == itemID
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Item by name.
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        private Item GetItem(string itemName)
        {
            return (from x in db_Game.Items
                    where x.name == itemName
                    select x).SingleOrDefault();
        }


        /// <summary>
        /// Get Armour Type by ID.
        /// </summary>
        /// <param name="armourTypeID"></param>
        /// <returns></returns>
        public ArmourType GetArmourType(int armourTypeID)
        {
            return (from x in db_Game.ArmourTypes
                    where x.armourTypeID == armourTypeID
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Armour Type by name.
        /// </summary>
        /// <param name="armourTypeName"></param>
        /// <returns></returns>
        public ArmourType GetArmourType(string armourTypeName)
        {
            return (from x in db_Game.ArmourTypes
                    where x.name == armourTypeName
                    select x).SingleOrDefault();
        }


        /// <summary>
        /// Get Damage Type by ID.
        /// </summary>
        /// <param name="damageTypeID"></param>
        /// <returns></returns>
        public DamageType GetDamageType(int damageTypeID)
        {
            return (from x in db_Game.DamageTypes
                    where x.damageTypeID == damageTypeID
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Damage Type by name.
        /// </summary>
        /// <param name="damageTypeName"></param>
        /// <returns></returns>
        public DamageType GetDamageType(string damageTypeName)
        {
            return (from x in db_Game.DamageTypes
                    where x.name == damageTypeName
                    select x).SingleOrDefault();
        }


        /// <summary>
        /// Get Weapon Type by Weapon Type ID.
        /// </summary>
        /// <param name="weaponTypeID"></param>
        /// <returns></returns>
        public WeaponType GetWeaponType(int weaponTypeID)
        {
            return (from x in db_Game.WeaponTypes
                    where x.weaponTypeID == weaponTypeID
                    select x).SingleOrDefault();
        }

        /// <summary>
        /// Get Weapon Damage Type by Damage Type ID.
        /// </summary>
        /// <param name="weaponTypeName"></param>
        /// <returns></returns>
        public WeaponType GetWeaponType(string weaponTypeName)
        {
            return (from x in db_Game.WeaponTypes
                    where x.name == weaponTypeName
                    select x).SingleOrDefault();
        }
        #endregion


        #region - Get All Methods.
        /// <summary>
        /// Get All Races.
        /// </summary>
        /// <returns></returns>
        public IList<Race> GetAllRaces()
        {
            return (from x in db_Game.Races
                    select x).ToList();
        }


        /// <summary>
        /// Get All Classes.
        /// </summary>
        /// <returns></returns>
        public IList<Class> GetAllClasses()
        {
            return (from x in db_Game.Classes
                    select x).ToList();
        }


        /// <summary>
        /// Get All Zones.
        /// </summary>
        /// <returns></returns>
        public IList<Zone> GetAllZones()
        {
            return (from x in db_Game.Zones
                    select x).ToList();
        }


        /// <summary>
        /// Get All Currencies.
        /// </summary>
        /// <returns></returns>
        public IList<Currency> GetAllCurrencies()
        {
            return (from x in db_Game.Currencies
                    select x).ToList();
        }

        /// <summary>
        /// Get All Currency Rates.
        /// </summary>
        /// <returns></returns>
        public IList<CurrencyConversionRate> GetAllCurrencyRates()
        {
            return (from x in db_Game.CurrencyConversionRates
                    select x).ToList();
        }


        /// <summary>
        /// Get all Item Types.
        /// </summary>
        /// <returns></returns>
        public IList<ItemType> GetAllItemTypes()
        {
            return (from x in db_Game.ItemTypes
                    select x).ToList();
        }

        /// <summary>
        /// Get all Item Tiers.
        /// </summary>
        /// <returns></returns>
        public IList<ItemTier> GetAllItemTiers()
        {
            return (from x in db_Game.ItemTiers
                    select x).ToList();
        }


        /// <summary>
        /// Get all Slot Types.
        /// </summary>
        /// <returns></returns>
        public IList<SlotType> GetAllSlotTypes()
        {
            return (from x in db_Game.SlotTypes
                    select x).ToList();
        }


        /// <summary>
        /// Get all Damage Types.
        /// </summary>
        /// <returns></returns>
        public IList<DamageType> GetAllDamageTypes()
        {
            return (from x in db_Game.DamageTypes
                    select x).ToList();
        }


        /// <summary>
        /// Get all Armour Types.
        /// </summary>
        /// <returns></returns>
        public IList<ArmourType> GetAllArmourTypes()
        {
            return (from x in db_Game.ArmourTypes
                    select x).ToList();
        }

        /// <summary>
        /// Get all Armour Weaknesses.
        /// </summary>
        /// <returns></returns>
        public IList<ArmourWeakness> GetAllArmourWeaknesses()
        {
            return (from x in db_Game.ArmourWeaknesses
                    select x).ToList();
        }

        /// <summary>
        /// Get all Armour Strengths.
        /// </summary>
        /// <returns></returns>
        public IList<ArmourStrength> GetAllArmourStrengths()
        {
            return (from x in db_Game.ArmourStrengths
                    select x).ToList();
        }


        /// <summary>
        /// Get all Armour Strengths.
        /// </summary>
        /// <returns></returns>
        public IList<WeaponType> GetAllWeaponTypes()
        {
            return (from x in db_Game.WeaponTypes
                    select x).ToList();
        }
        
        /// <summary>
        /// Get all Weapon Damage Types.
        /// </summary>
        /// <returns></returns>
        public IList<WeaponDamageType> GetAllWeaponDamageTypes()
        {
            return (from x in db_Game.WeaponDamageTypes
                    select x).ToList();
        }


        /// <summary>
        /// Get all Items.
        /// </summary>
        /// <returns></returns>
        public IList<Item> GetAllItems()
        {
            return (from x in db_Game.Items
                    select x).ToList();
        }

        /// <summary>
        /// Get all Item Stats.
        /// </summary>
        /// <returns></returns>
        public IList<ItemStat> GetAllItemStats()
        {
            return (from x in db_Game.ItemStats
                    select x).ToList();
        }
        #endregion


        #region - Get By Methods.
        /// <summary>
        /// Get Items by type ID.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private IList<Item> GetItemsOfType(int itemType)
        {
            return (from x in db_Game.Items
                    where x.itemTypeID == itemType
                    select x).ToList();
        }


        /// <summary>
        /// Get Items by type name.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private IList<Item> GetItemsOfType(string itemType)
        {
            return (from x in db_Game.Items
                    where x.ItemType.name == itemType
                    select x).ToList();
        }


        /// <summary>
        /// Get Items by tier ID.
        /// </summary>
        /// <param name="itemTier"></param>
        /// <returns></returns>
        private IList<Item> GetItemsOfTier(int itemTier)
        {
            return (from x in db_Game.Items
                    where x.itemTierID == itemTier
                    select x).ToList();
        }


        /// <summary>
        /// Get Items by tier name.
        /// </summary>
        /// <param name="itemTier"></param>
        /// <returns></returns>
        private IList<Item> GetItemsOfTier(string itemTier)
        {
            return (from x in db_Game.Items
                    where x.ItemTier.name == itemTier
                    select x).ToList();
        }
        

        /// <summary>
        /// Get all Weapon Damage Types by Weapon Type ID.
        /// </summary>
        /// <param name="weaponTypeID"></param>
        /// <returns></returns>
        public IList<WeaponDamageType> GetAllWeaponDamageTypesByWeaponType(int weaponTypeID)
        {
            return (from x in db_Game.WeaponDamageTypes
                    where x.weaponTypeID == weaponTypeID
                    select x).ToList();
        }

        /// <summary>
        /// Get all Weapon Damage Types by Damage Type ID.
        /// </summary>
        /// <param name="damageTypeID"></param>
        /// <returns></returns>
        public IList<WeaponDamageType> GetAllWeaponDamageTypesByDamageType(int damageTypeID)
        {
            return (from x in db_Game.WeaponDamageTypes
                    where x.damageTypeID == damageTypeID
                    select x).ToList();
        }
        #endregion



        #region - Create Methods.
        public bool CreateRace(string raceName)
        {
            Race race = new Race
            {
                raceType = raceName
            };


            if (db_Game.Races.Add(race) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateClass(string className)
        {
            Class Class = new Class
            {
                className = className
            };


            if (db_Game.Classes.Add(Class) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateZone(string zoneName, string description)
        {
            Zone zone = new Zone();

            zone.name = zoneName;
            if (string.IsNullOrEmpty(description) == false)
            {
                zone.description = description;
            }

            if (db_Game.Zones.Add(zone) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateCurrency(string currencyName)
        {
            Currency Currency = new Currency
            {
                name = currencyName
            };


            if (db_Game.Currencies.Add(Currency) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateCurrencyRate(int currencyFrom, int currencyTo, float currencyRate)
        {
            CurrencyConversionRate CurrencyRate = new CurrencyConversionRate
            {
                rate = (decimal)currencyRate,

                currencyID = currencyFrom,
                otherCurrencyID = currencyTo
            };


            if (db_Game.CurrencyConversionRates.Add(CurrencyRate) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateCurrencyRate(string currencyFromName, string currencyToName, float currencyRate)
        {
            CurrencyConversionRate CurrencyRate = new CurrencyConversionRate
            {
                rate = (decimal)currencyRate,

                currencyID = GetCurrency(currencyFromName).currencyID,
                otherCurrencyID = GetCurrency(currencyToName).currencyID
            };


            if (db_Game.CurrencyConversionRates.Add(CurrencyRate) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateItemType(string itemTypeName)
        {
            ItemType itemType = new ItemType
            {
                name = itemTypeName
            };


            if (db_Game.ItemTypes.Add(itemType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateItemTier(string itemTierName)
        {
            ItemTier itemTier = new ItemTier
            {
                name = itemTierName
            };


            if (db_Game.ItemTiers.Add(itemTier) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateSlotType(string slotTypeName)
        {
            SlotType slotType = new SlotType
            {
                name = slotTypeName
            };


            if (db_Game.SlotTypes.Add(slotType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateDamageType(string damageTypeName)
        {
            DamageType damageType = new DamageType
            {
                name = damageTypeName
            };


            if (db_Game.DamageTypes.Add(damageType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateArmourType(string armourTypeName)
        {
            ArmourType armourType = new ArmourType
            {
                name = armourTypeName
            };


            if (db_Game.ArmourTypes.Add(armourType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateArmourWeakness(int armourID, int weaknessID, float weaknessAmount)
        {
            ArmourWeakness armourWeakness = new ArmourWeakness
            {
                armourTypeID = armourID,
                damageTypeID = weaknessID,
                amount = (decimal)weaknessAmount
            };


            if (db_Game.ArmourWeaknesses.Add(armourWeakness) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateArmourStrength(int armourID, int strengthID, float strengthAmount)
        {
            ArmourStrength armourStrength = new ArmourStrength
            {
                armourTypeID = armourID,
                damageTypeID = strengthID,
                amount = (decimal)strengthAmount
            };


            if (db_Game.ArmourStrengths.Add(armourStrength) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }


        public bool CreateWeaponType(string weaponTypeName)
        {
            WeaponType weaponType = new WeaponType
            {
                name = weaponTypeName
            };


            if (db_Game.WeaponTypes.Add(weaponType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CreateWeaponDamageType(int weaponTypeID, int damageTypeID)
        {
            WeaponDamageType weaponDamageType = new WeaponDamageType
            {
                weaponTypeID = weaponTypeID,
                damageTypeID = damageTypeID
            };


            if (db_Game.WeaponDamageTypes.Add(weaponDamageType) != null)
            {
                try
                {
                    db_Game.SaveChanges();
                }
                catch (Exception e)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }
}