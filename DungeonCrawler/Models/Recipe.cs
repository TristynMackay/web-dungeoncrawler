//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DungeonCrawler.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Recipe
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Recipe()
        {
            this.AlchemyRecipes = new HashSet<AlchemyRecipe>();
            this.ArmoursmithingRecipes = new HashSet<ArmoursmithingRecipe>();
            this.CookingRecipes = new HashSet<CookingRecipe>();
            this.CraftingReagents = new HashSet<CraftingReagent>();
            this.EnchantingRecipes = new HashSet<EnchantingRecipe>();
            this.FishingRecipes = new HashSet<FishingRecipe>();
            this.HerbalismRecipes = new HashSet<HerbalismRecipe>();
            this.JewelcraftingRecipes = new HashSet<JewelcraftingRecipe>();
            this.LeatherworkingRecipes = new HashSet<LeatherworkingRecipe>();
            this.MiningRecipes = new HashSet<MiningRecipe>();
            this.RunecraftingRecipes = new HashSet<RunecraftingRecipe>();
            this.SkinningRecipes = new HashSet<SkinningRecipe>();
            this.TailoringRecipes = new HashSet<TailoringRecipe>();
        }
    
        public int recipeID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int requiredLevel { get; set; }
        public int experienceGain { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlchemyRecipe> AlchemyRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ArmoursmithingRecipe> ArmoursmithingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CookingRecipe> CookingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CraftingReagent> CraftingReagents { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EnchantingRecipe> EnchantingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FishingRecipe> FishingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HerbalismRecipe> HerbalismRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JewelcraftingRecipe> JewelcraftingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeatherworkingRecipe> LeatherworkingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MiningRecipe> MiningRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RunecraftingRecipe> RunecraftingRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SkinningRecipe> SkinningRecipes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TailoringRecipe> TailoringRecipes { get; set; }
    }
}
