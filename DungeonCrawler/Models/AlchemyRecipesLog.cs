//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DungeonCrawler.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AlchemyRecipesLog
    {
        public int alchemyRecipeLogID { get; set; }
        public int alchemyRecipeID { get; set; }
        public int characterID { get; set; }
    
        public virtual AlchemyCraft AlchemyCraft { get; set; }
        public virtual AlchemyRecipe AlchemyRecipe { get; set; }
    }
}
