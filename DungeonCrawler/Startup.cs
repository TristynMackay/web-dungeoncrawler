﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DungeonCrawler.Startup))]
namespace DungeonCrawler
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
